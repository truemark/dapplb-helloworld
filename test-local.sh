#!/usr/bin/env bash

cd "$(dirname "${0}")" || exit 1

docker run -v "$(pwd):/live" -i --rm truemark/dapplb:debian apply /live/test-local-config.json
