#!/usr/bin/env bash

cd "$(dirname "${0}")" || exit 1

docker run -i --rm truemark/dapplb:debian apply "https://bitbucket.org/truemark/dapplb-helloworld/raw/master/test-remote-config.json"
